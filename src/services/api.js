import axios from "axios"

const api = axios.create({
    baseURL: "https://api.github.com/users/matheuspsantos/repos"
});

export default api;