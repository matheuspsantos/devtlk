import React, { Component, Fragment } from "react";
import api from "../../services/api";

import Input from "../Input";
import Button from "../Button";

import "./styles.css";

class List extends Component {

    state = {
        repos: [],
        filter: "",
    };

    componentDidMount() {
        this.loadRepos();
    }

    loadRepos = async () => {
        const url = `${ this.state.filter}/repos`;
        const response = await api.get(url);
        this.setState({ repos: response.data });
    };

    handleButtonClick = () => {
        this.loadRepos();
    };

    handleChangeInput = event => {
        this.setState({ filter: event.target.value });
    };

    render() {
        return (
            <Fragment>
                <h1>Lista de Repositórios {this.state.filter} </h1>
                <div className="repos-filter">
                    <Input label="Digite um usuário..." onChange={this.handleChangeInput} />
                    <Button onClick={this.handleButtonClick}>Filtrar</Button>
                </div>
                <ul>
                    {(this.state.repos.length && this.state.repos.map((item, key) => {
                        return (
                            <li key={key}>
                                <a href={item.html_url} target="_blank" rel="noopener noreferrer">
                                    {item.name}
                                </a>
                            </li>
                        );
                    })) || <li>Carregando...</li>}
                </ul>
            </Fragment>
        );
    }
}

export default List;