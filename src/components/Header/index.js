import React from "react";
import "./index.css";

const Header = () => {
    return (
        <header id="main-header">Git app</header>
    );
};

export default Header;