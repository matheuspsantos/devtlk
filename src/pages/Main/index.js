import React, { Fragment } from "react";
import Header from "../../components/Header";
import Clock from "../../components/Clock";
import List from "../../components/List";
import "./index.css"

const Main = () => {
    return (
        <Fragment>
            <Header/>
            <List/>
            <Clock/>
        </Fragment>
    );
};

export default Main;